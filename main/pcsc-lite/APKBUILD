# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=pcsc-lite
pkgver=2.2.0
pkgrel=0
pkgdesc="Middleware to access a smart card using SCard API (PC/SC)"
url="https://pcsclite.apdu.fr/"
arch="all"
license="BSD-3-Clause AND BSD-2-Clause AND ISC"
depends_dev="
	$pkgname-libs=$pkgver-r$pkgrel
	eudev-dev
	"
makedepends="
	$depends_dev
	flex
	libcap-ng-utils
	meson
	perl-dev
	"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="
	$pkgname-static
	$pkgname-dev
	$pkgname-doc
	$pkgname-spy-libs:spylibs
	$pkgname-libs
	$pkgname-openrc
	"
source="https://pcsclite.apdu.fr/files/pcsc-lite-$pkgver.tar.xz
	include-prefix-in-sbindir.patch
	pcscd.initd
	"

build() {
	abuild-meson . build \
		-D default_library=both \
		-D usb=false \
		-D libudev=true \
		-D polkit=false \
		-D libsystemd=false

	meson compile -C build
}

check() {
	meson test --no-rebuild --print-errorlogs -C build
}

package() {
	license="$license AND GPL-3.0-or-later"

	meson install --no-rebuild --destdir "$pkgdir" -C build

	install -D -m755 "$srcdir"/pcscd.initd "$pkgdir"/etc/init.d/pcscd

	mkdir -p "$pkgdir"/usr/lib/pcsc/drivers
}

spylibs() {
	pkgdesc="$pkgdesc (log/debug/spy libraries)"
	license="GPL-3.0-or-later"

	amove usr/lib/libpcscspy.so.*
}

libs() {
	pkgdesc="$pkgdesc (libraries)"

	amove usr/lib
}

dev() {
	default_dev

	# move back the /usr/lib/libpcsclite.so
	# see http://bugs.alpinelinux.org/issues/3236 (and 6392)
	mv "$subpkgdir"/usr/lib/libpcsclite.so "$pkgdir"/usr/lib/libpcsclite.so
}

sha512sums="
07771a45f92b472bc0085c992da4c8a031d4ccadca592a515c50e5ce240bca90111f4e33b759fc24ba42c05426b8c9fe320d8d55bbd6392b037f6f4ca9480b02  pcsc-lite-2.2.0.tar.xz
b13bba9bc4aab3801bb4a9fb366dd1bd117bc75c89eb22871b9feb4bd79bfcc79a6bb33c7a11d6fea8b421ce5c540f3ed62fba10550fafb32d811924467c0b66  include-prefix-in-sbindir.patch
5a8b3cb5b4ed9884c1032ec3c4c51167b8771185e8ea4122e40de4a92135cc3687a1df43bc877cf5ebcfe9392a17035873d8c052d58243eb681a3e93a44c5773  pcscd.initd
"
