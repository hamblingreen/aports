# Contributor: Noel Kuntze <noel.kuntze@thermi.consulting>
# Maintainer: Noel Kuntze <noel.kuntze@thermi.consulting>
pkgname=py3-daemon
pkgver=2.3.2
pkgrel=4
pkgdesc="Library to implement a well-behaved Unix daemon process"
url="https://pagure.io/python-daemon"
options="!check" # Has lots of dependencies
arch="noarch"
license="Apache-2.0 AND GPL-3.0-or-later"
depends="py3-setuptools py3-lockfile"
makedepends="py3-docutils"
subpackages="$pkgname-pyc"
source="
	https://pypi.io/packages/source/p/python-daemon/python-daemon-$pkgver.tar.gz
	remove-docutils-depend.patch
"
builddir="$srcdir/python-daemon-$pkgver"

prepare() {
	default_prepare

	# Remove unnecessary dependency for building, twine is
	# required for uploading the package to pypi which we don't
	# do
	sed -e '/twine/d' -i setup.py
}

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --root="$pkgdir"
}

sha512sums="
d9f6e6c376a496fae96bd9efed0a56d00a137617a3d1d5ef74802ef176bc813bb1d49bbb9164cdbec03213529f944b32b257bcc64283abfa4a3522ff00826bfd  python-daemon-2.3.2.tar.gz
20bc4bce7fba9754d1c9bca298f9b4ff7fe90ecf51769df2020912f370650a5997f91c99c6be2ecffed10a6b359a29b1f64b0c391772451eb3c04769df381015  remove-docutils-depend.patch
"
